Source: wpebackend-fdo
Section: libs
Priority: optional
Maintainer: Debian WebKit Maintainers <pkg-webkit-maintainers@lists.alioth.debian.org>
Uploaders: Alberto Garcia <berto@igalia.com>
Build-Depends: debhelper-compat (= 12),
               cmake,
               libegl1-mesa-dev,
               libglib2.0-dev,
               libwayland-dev,
               libwpe-1.0-dev (>= 1.5.90)
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/webkit-team/wpebackend-fdo
Vcs-Git: https://salsa.debian.org/webkit-team/wpebackend-fdo.git
Homepage: https://wpewebkit.org/

Package: libwpebackend-fdo-1.0-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libwpebackend-fdo-1.0-1 (= ${binary:Version}),
         libwpe-1.0-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Description: WPE backend for FreeDesktop.org - Development files
 WPE WebKit is a port of the WebKit browser engine optimized for
 embedded devices. The code itself is hardware-agnostic, and the
 platform-specific bits are implemented in backends.
 .
 wpebackend-fdo provides a reference WPE backend implementation using
 FreeDesktop.org technologies (Wayland protocol and the Wayland EGL
 platform) to enable integration into the WPE WebKit process model.
 .
 This package contains the development files.

Package: libwpebackend-fdo-1.0-1
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: WPE backend for FreeDesktop.org
 WPE WebKit is a port of the WebKit browser engine optimized for
 embedded devices. The code itself is hardware-agnostic, and the
 platform-specific bits are implemented in backends.
 .
 wpebackend-fdo provides a reference WPE backend implementation using
 FreeDesktop.org technologies (Wayland protocol and the Wayland EGL
 platform) to enable integration into the WPE WebKit process model.
 .
 This package contains the shared libraries.
